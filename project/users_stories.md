# User stories

1. Single user starts a game and insert letters to fill a previously defined hidden word.
2. Single user starts a game and insert letters to fill a previously defined hidden word and when surpasses 7 tries he loses the game.
3. Single user starts a game and insert letters to fill a previously defined hidden word. The user may recieve "lose", "win", or "playing" status message after: guessing all letter right, surpassing 7 tries or none of both.
4. for each letter in word a "_" shows up every try