# Game Rules


 -> Largura da tela: uma barra de espaço + 41 caracteres
 -> A cada rodada existem 2 turnos (O que o Player 1 tenta adivinhar e o do Player 2)
 -> O jogador só ganha pontos quando adivinhar uma palavra ou preencher todas as letras dela (acertar)
 -> Player 1 e Player 2 são os nomes padrão dos jogadores e não podem ser mudados
 -> A barra de Status pode ter as seguintes situações:
  - Type "play" to start playing
  - Type number of Rounds you want to play  
  - Try first letter
  - Got it! type "w" to guess word, "l" to try letter
  - You missed! type "w" to guess word, "l" to try letter
  - Won!! (type "ok" to continue)
  - Lost... (type "ok" to continue)
  - Player X wins the game!!!
 -> As palavras à serem adivinhadas podem ter no máximo 10 letras
 -> O máximo de tentativas possíveis são 7
 -> A barra "Type" só aceita o que é pedido para digitar (numero;letra;"ok";"play";"guess") 

 