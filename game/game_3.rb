class Game
  def initialize
    @hangman = Hangman.new    
  end

  def play
    while @hangman.can_play?
      update_screen

      print " Try a letter: "
      letter = gets.to_s.chomp

      @hangman.try_letter!(letter[0].downcase)
    end

    show_final_screen
  end  

  private

  def update_screen
    HangmanPrinter.update_screen(@hangman,@hangman.failures_count, @hangman.failed_attempts)    
  end  

  def show_final_screen
    update_screen

    HangmanPrinter.width_respect_print("Result: #{@hangman.status}")

    HangmanPrinter.width_respect_print("The word was \"#{@hangman.word}\"")
  end  
end

class Hangman
  attr_reader :status, :word, :letters_to_find, :failures_count, :failed_attempts

  POSSIBLE_WORDS = File.read("word_list.txt").split
  MAX_FAILURES = 7

  def initialize
    @word = POSSIBLE_WORDS.sample
    @letters_to_find = @word.split("")
    @failed_attempts = Array.new
    @status = :playing
    @failures_count = 0
  end

  def can_play?
    @status == :playing
  end

  def try_letter!(letter)
    deleted_letter = @letters_to_find.delete(letter)

    if deleted_letter.nil?
      @failed_attempts.append(letter)
      @failures_count += 1
    end

    update_status
  end

  private

  def update_status
    if @letters_to_find.empty?
      @status = :won
    elsif @failures_count >= MAX_FAILURES
      @status = :lost
    end
  end
end 

class HangmanPrinter

  SCREEN_WIDTH = 41
  SCREEN_HEAD = " ############### Hangman ############### "
  

  def self.width_respect_print(string)
    print " "*((SCREEN_WIDTH - string.length)/2)
    puts string
  end

  def self.update_screen(hangman, failures_count, failed_attempts)
    puts `clear`

    puts SCREEN_HEAD + "\n\n"

    HangmanPrinter.print_word(hangman)
    HangmanPrinter.print_doll(failures_count)
    HangmanPrinter.print_failed_attempts(failed_attempts)
  end

  private

  def self.print_failed_attempts(failed_attempts)
    @@failed_attempts_layout = File.read("failed_attempts.txt").split("\n")

    if failed_attempts != 0
      var = 0
      failed_attempts.each do |attempt|
        @@failed_attempts_layout[1][21 + var*2] = attempt.upcase
        var += 1
      end
    end

    @@failed_attempts_layout.each do |line|
      puts line
    end  
      
  end  

  def self.print_doll(failures)  
   @@screen_doll = File.read("gallows_layout.txt").split("#")[failures]
   puts @@screen_doll
  end

  def self.print_word(hangman)
    screen_word = hangman.word.clone
    screen_word = screen_word.gsub!(/./) {|letter| ' ' + letter + ' '}

    print " "*((SCREEN_WIDTH - screen_word.length)/2)
    hangman.letters_to_find.each do |letter|
      screen_word.gsub!(letter, "_")      
    end

    puts screen_word
  end  
end

Game.new.play