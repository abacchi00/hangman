class Hangman
  def initialize(word)
    @word = word
    @letters = word.split("")
  end  
end

class Guesser
  def initialize(word_length)
    @known_letters = Array.new(word_length, "")
    @known_word = @known_letters.join()
  end

  def refresh_known(letter,positions)
    
  end  

  def show_known_letters
  end  
end

class Game
  def initialize
    @hangman = Hangman.new("abacaxi")
    @guesser = Guesser.new(@hangman.word.length)
    @status = "Playing"
    @tries = 0
  end

  def refresh_status
    if @guesser.known_word == @hangman.word
      @status = "Victory"
    elsif @tries < 8
      @status = "Playing"
    else
      @status = "Defeat"
    end
  end  

  def check_letter(letter)
    for i in 0...@hangman.word.length
      if letter == @hangman.word[i]
        return true
      end  
    end
    return false
  end

  def check_positions
    positions = Array.new

    for i in 0...@hangman.word.length
      if letter == @hangman.word[i]
        positions.push(i)
      end  
    end

    return positions
  end  

  def try_letter
    print "Enter a letter: "
    letter = gets.to_s.chomp

    if check_letter(letter)
      return letter
    else
      return nil
    end    
  end

  def play
    while @status == "Playing"
      puts "Status: #{@status}"
      letter_hit = try_letter
      positions =

      if letter_hit != nil
        @guesser.refresh_known(letter_hit, check_positions)
      end
      @guesser.show_known_letters
      
      tries += 1

      refresh_status
    end  
  end
end

Game.new.play