class Game
  def initialize
    @hangman = Hangman.new    
  end

  def play
    while @hangman.can_play?
      HangmanPrinter.update_screen(@hangman,@hangman.failures_count)

      print " Try a letter: "
      letter = gets.to_s.chomp

      @hangman.try_letter!(letter)
    end

    HangmanPrinter.update_screen(@hangman,@hangman.failures_count)
    puts " Result: #{@hangman.status}"
  end  
end

class Hangman
  attr_reader :status, :word, :letters_to_find, :failures_count

  MAX_FAILURES = 7

  def initialize
    @word = "abacaxi"
    @letters_to_find = @word.split("")
    @status = :playing
    @failures_count = 0
  end

  def can_play?
    @status == :playing
  end

  def try_letter!(letter)
    deleted_letter = @letters_to_find.delete(letter)

    @failures_count += 1 if deleted_letter.nil? 

    update_status
  end

  private

  def update_status
    if @letters_to_find.empty?
      @status = :won
    elsif @failures_count >= MAX_FAILURES
      @status = :lost
    end
  end
end 

class HangmanPrinter

  DOLL = ["O", "┼", "|", "*─", "─*", "_/", "\\_"]
  @@doll = [" ", " ", " ", "  ", "  ", "  ", "  "]
  

  def self.update_screen_doll  
    @@screen_doll =  [" ┌───────────────────┐", 
                      " |     ╔══════╗      |",
                      " |     ║      #{@@doll[0]}      |",
                      " |     ║    #{@@doll[3]}#{@@doll[1]}#{@@doll[4]}    |",
                      " |     ║      #{@@doll[2]}      |",
                      " |     ║    #{@@doll[5]} #{@@doll[6]}    |",
                      " |     ║             |",
                      " |   ══╩══           |",
                      " └───────────────────┘"]
  end

  def self.print_doll(failures_count)
    for i in [0...failures_count]
      @@doll[i] = DOLL[i] 
    end

    HangmanPrinter.update_screen_doll
    puts @@screen_doll
  end  

  def self.update_screen(hangman, failures_count)
    puts `clear`

    screen_word = hangman.word.clone

    print " "

    hangman.letters_to_find.each do |letter|
      screen_word.gsub!(letter, " _ ")      
    end

    puts screen_word
    HangmanPrinter.print_doll(failures_count)
  end
end

Game.new.play